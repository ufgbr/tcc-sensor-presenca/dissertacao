\section{Visão computacional}

Os sistemas de visão biológica são notáveis na evolução das espécies,
sobretudo, por serem responsáveis por extrair informações importantes do
meio necessárias para a navegação em ambientes  complexos, busca de alimento e fuga de predadores. Esses sistemas biológicos executam
todas essas tarefas com muita precisão e sem falhas, mesmo em ambientes naturais variados
e ambíguos. Além disso, podem resolver com eficiência e rapidez problemas complexos
como percepção 3D, segmentação de cenários, identificação de objetos e movimento. \cite{bio-inspired}

De modo comparável, a visão computacional é a área de estudo que tem em vista
replicar a visão humana em que computadores podem interpretar e analisar imagens
e vídeos e, a partir disso, extrair informações relevantes dessas fontes. Do ponto
de vista humano, desde o nascimento os indivíduos aprendem a perceber o meio pelos
denominados 5 sentidos (visão, olfato, paladar, audição e tato), mas é a visão,
principalmente, a responsável por identificar objetos e contextos. Com o objetivo de
conseguir os mesmos resultados, a visão computacional busca alcançá-los em menos
tempo, o que requer o desenvolvimento de algoritmos e modelos que conseguem
analisar essas imagens da mesma forma que o homem. As aplicações de visão
computacional incluem análise de imagem e vídeo, detecção de objetos,
reconhecimento facial, entre outros.

Enquanto campo de estudo, a visão computacional começou a tomar forma em 1960 no MIT\footnote{
    Massachusetts Institute of Technology.
    MIT é uma universidade privada de pesquisa localizada em Cambridge, Massachusetts, Estados Unidos.
}
com Larry Roberts em seu trabalho de pós-doutorado \cite[pág. 1]{computer-vision-evolution}.
Na época, ele discutia a possibilidade de extrair informações 3D de objetos em
imagens 2D. Em 1970, David Marr propôs que o processo de ver poderia ser dividido
em três níveis: o esboço primário, o esboço 2.5D e o modelo 3D, cada um
correspondente a um nível diferente de abstração e complexidade computacional.
Nesse contexto, os computadores não eram poderosos o suficiente para processar
grandes quantidades de dados visuais, então as primeiras pesquisas no campo focaram
em desenvolver técnicas para analisar e compreender imagens usando operações
matemáticas e estatísticas.

Hoje, a visão computacional é um campo em acelerado crescimento que abrange uma
ampla gama de técnicas e tecnologias, desde técnicas simples de processamento de
imagem a modelos complexos de \textit{deep learning}.


\subsection{Detecção de alunos}


Para este trabalho serão consideradas duas abordagens para a solução de dois
problemas: a detecção de alunos e a classificação do aluno. A primeira abordagem
está dentro do camera presence, que na arquitetura adotada é o módulo responsável
por detectar os alunos durante a aula. No pipeline desse módulo, como mostra a \autoref{camera-presence-pipeline},
há a etapa de Detecção de Rostos responsável por detectar os rostos dos
alunos em um determinado frame
\footnote{
    Frame é uma imagem pertencente a um conjunto de imagens que juntos e em sequencia
    forma o vídeo.
}
do feed da câmera.

\begin{figure}[htb]
    \caption{\label{camera-presence-pipeline} Pipeline Modulo Camera Presence}
    \begin{center}
        \includegraphics[scale=0.7]{./fig/camera_presence_pipeline.png}
    \end{center}
    \legend{Fonte: Acervo pessoal}
\end{figure}

Nessa etapa usamos o algoritmo de detecção de objetos Haar feature-based Cascade
proposto por \citeonline{hars-cascade}. Esse modelo já vem implementado
na biblioteca OpenCV na função CascadeClassifier, além disso, foi utilizado o conjunto
de pesos treinado para detecção de faces\footnote{
    Disponível em: \url{https://github.com/opencv/opencv/tree/master/data/haarcascades}
}. A técnica é baseada em uma série de detectores de características simples,
chamados filtros Haar, usados para criar uma cascata de classificadores que
podem ser aplicados a uma imagem em uma série de estágios. Em cada estágio, o
cascade classifier aplica esses filtros (ver a Figura 5) à imagem e usa os
resultados para decidir se deve ou não seguir para a próxima etapa. Os filtros
do tipo Haar são retangulares simples que podem ser aplicados a uma
imagem em um estilo de janela deslizante e calculados subtraindo a soma dos
pixeis em um retângulo da soma dos pixeis em um retângulo vizinho.

\begin{figure}[htb]
    \caption{\label{haar-features} Exemplo de features retangulares}
    \begin{center}
        \includegraphics[scale=0.7]{./fig/haar-features.png}
    \end{center}
    \legend{Fonte: \citeonline{hars-cascade}}
\end{figure}

A cascata de classificadores é treinada usando um grande conjunto de dados de
imagens positivas e negativas\footnote{
    Imagens positivas são imagens que contem os objetos a serem detectados, a as
    imagens negativas são imagens que não contem o objeto a ser detectado.
}
, como um conjunto de dados de faces e não faces.
Durante o processo de treinamento, os classificadores são otimizados para
minimizar o número de falsos positivos e falsos negativos. Isso resulta em
uma série de classificadores que podem ser aplicados a uma imagem de forma
hierárquica, em que cada estágio da cascata aumenta o número de características
e reduz o número de falsos positivos.

Em seguida, na etapa de Tracking Rostos, é criado um tracker para cada rosto
encontrado, visando minimizar a quantidade de imagens publicadas pelo módulo
camera presence. Para isso, foi utilizado a biblioteca dlib que foi desenvolvida
para resolver problemas de processamento de imagens e visão computacional.
Entre as suas funcionalidades mais populares encontra-se a de rastreamento
presente no método \textit{correlation tracker} que implementa a solução proposta por
\citeonline{accurate-scale}. A ideia principal do algoritmo é baseado na  relação
entre uma imagem de referência e um objeto. Ele também usa a técnica janela deslizante
para comparar as imagens, o que significa que desloca uma pequena janela sobre a imagem
atual para encontrar o melhor ajuste com a imagem de referência. A localização da janela
que produz o melhor ajuste é então considerada a posição atual do objeto.


\subsection{Classificação do aluno}


Após a identificação das faces, o módulo attendance captura as imagens obtidas
pelo camera presence via MQTT e inicia o processo de identificação dos alunos.
É nessa etapa que a segunda abordagem opera, no módulo attendance utilizamos a
biblioteca DeepFace para verificar se a imagem recebida é de um aluno cadastrado
naquela turma, após a verificação é feita a frequência parcial desse aluno.
Desta maneira, o DeepFace oferece o método verify que recebe como parâmetro duas
imagens e retorna o quão similar as duas faces são, sendo similaridade pode ser
calculada de duas formas: distância euclidiana ou semelhança de cossenos. A forma
de calcular selecionada para este projeto foi a distância euclidiana. A Distância
Euclidiana é definida como a soma da raiz quadrada da diferença entre
\textit{p} e \textit{q} em suas respectivas dimensões(\autoref{distancia-euclidiana}).

\begin{figure}[htb]
    \caption{\label{distancia-euclidiana} Distância euclidiana entre dois pontos}
    \begin{center}
        \includegraphics[scale=1]{./fig/distancia-euclidiana.png}
    \end{center}
    \legend{Fonte: \url{https://pt.wikipedia.org/wiki/Distância_euclidiana}}
\end{figure}

Além disso, é possível escolher qual modelo será utilizado pelo método verify. Dentre
os modelos disponíveis optamos pelo Google FaceNet proposto por \citeonline{FaceNet}.
Esse modelo de rede neural convolucional
\footnote{
    Etapa de pré-processamento em uma rede neural.
}
profunda  foi idealizado para cenários de verificação facial, a rede foi
treinada de modo que a distância euclidiana no espaço entre duas imagens
corresponda diretamente com a similaridade entre as faces. O output
\footnote{Resultado produzido pelo modelo ao receber uma imagem.} da rede
neural é um vetor de 128 características, que podem ter qualquer significado,
mas que representa a interpretação da rede neural sobre o rosto. Isso pode ser
visto na \autoref{googlenet-faces-distance}, na qual pode ser percebida que a
distância entre a face de duas pessoas diferentes é maior que a distância entre
a face pertencente a mesma pessoa (distância menor que 1.04\footnote{
    Distância mínima para que o DeepFace considere que as duas imagens são da mesma pessoa.
}).

\begin{figure}[htb]
    \caption{\label{googlenet-faces-distance} Faces e distância euclidiana}
    \begin{center}
        \includegraphics[scale=0.6]{./fig/facenet-euclidean-distance.png}
    \end{center}
    \legend{Fonte: \citeonline{FaceNet}}
\end{figure}


A partir das imagens obtidas no camera presence cada aluno detectado é
comparado com um banco de imagens com todos os alunos daquela turma.
Para isso, para cada imagem de aluno salva no banco de dados, é feita
a comparação com a imagem recebida, utilizando a função \textit{verify} do DeepFace.
Essa função recebe as duas imagens e as pré-processa fazendo o redimensionamento para passar como input
\footnote{
    Argumentos de entrada do modelo.
}
na FaceNet. Por último, é calculada a distância entre os dois vetores e assumido
um threshold
\footnote{
    Valor mínimo para ser considerado verdadeiro.
} que verifica se as imagens são da mesma pessoa.