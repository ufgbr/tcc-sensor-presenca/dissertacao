Neste capítulo serão discutidos os detalhes do desenvolvimento dos microsserviços
que compõem o sistema de presença. Cada componente será separado em suas
principais partes, assim como elucidados as escolhas técnicas e de implementação. Os
códigos que serão apresentados são partes do código original do projeto,
todavia, para facilitar a leitura, foram subtraídos trechos que não são
relevantes para o entendimento do funcionamento do sistema, como inicialização
de variáveis, validações de parâmetros e tratamento de erros.

\section{Camera Presence}
A primeira parte do sistema é o microsserviço \textit{camera presence}, responsável
por capturar as imagens das câmeras e, a partir dessas imagens, conseguir
identificar a presença de alunos, para então enviar a imagem das faces detectadas
para outro microsserviço (\autoref{camera_presence_system}).

O desenvolvimento desse microsserviço foi pensado na ideia de
especialização de tarefas, ou seja, cada microsserviço deve ter uma única
atribuição e não pode ser responsável por mais de uma tarefa. Além disso,
ele foi projetado de forma que possa ser usado em outras aplicações - por exemplo,
em um sistema de portaria eletrônica o \textit{camera presence}
pode ser utilizado sem nenhuma modificação.

\begin{figure}[H]
    \caption{\label{camera_presence_system}
        Funcionamento básico do camera presence
    }
    \begin{center}
        \includegraphics[scale=0.8]{./fig/diagrams/diagram-camera_presence_system.png}
    \end{center}
    \legend{Fonte: Acervo pessoal}
\end{figure}

\subsection{Configuração}
Para a configuração do \textit{camera presence} foi criado
um arquivo de configuração \textit{yaml}. O padrão yaml foi preferido por
se tratar de um formato de configuração simples e de fácil entendimento, além
de ser bastante empregado em diversos projetos python.

Assim, ter um arquivo de configuração está diretamente ligado a escolha do uso de
microsserviços, uma vez que possibilita conter diferentes configurações para
diferentes ambientes, como desenvolvimento, homologação e produção,
sem a necessidade de alterar o código fonte.

No \textit{camera presence}, o arquivo yaml permite que seja
especificado para toda a aplicação informações como o hostname, onde
será executado o serviço, o modo de paralelismo escolhido, a
quantidade de câmeras que o serviço será responsável, tal como as especificações
das configurações individuais de cada uma delas.

Outra vantagem da utilização dos arquivos de configuração está na possibilidade
de definir um \textit{schema} para o arquivo, o que propicia que ele seja validado
antes de ser usado, evitando, dessa maneira, que o serviço seja iniciado com
configurações inválidas e bugs que podem ser difíceis de serem
encontrados.

% \begin{minipage}{\textwidth}
\begin{figure}[H]
    \caption{\label{camera-presence-config} Arquivo de configuração do \textit{camera presence}}
    \begin{minted}[linenos,xleftmargin=20pt]{yaml}
service_hostname: localhost
parallel_mode: thread
feeds:
    camera_1:
        backend: 'opencv'
        debug: true
        capture:
            type: 'rtsp'
            path: 'rtsp://admin:admin@192.168.1.122:554/h265Preview'
            fps: 15
        web:
            enabled: false
            resolution: [640, 480]
            fps: 5
        mqtt:
            enabled: true
            broker_host: 'mqtt-server'
            broker_port: 1883

        write_to_disk:
            enabled: true
            path: '/workspace/output_images/'

    \end{minted}
    % \legend{Fonte: Acervo pessoal}
\end{figure}
% \end{minipage}

A configuração de exemplo apresentada está configurada para apenas uma câmera
(feed), denominada no exemplo de \textit{camera\_1}. Dentro dela estão presentes as especificações de captura que definem como
a aplicação obterá as imagens da câmera, as configurações de web que
definem se a aplicação disponibilizará uma interface web para visualização
das imagens capturadas, as configurações de MQTT que definem se a aplicação
publicará essas imagens em um tópico (MQTT) e as configurações
de escrita em disco que definem se a aplicação salvará as imagens
capturadas em um diretório local.

\subsection{Multi Feed}

Apesar da abordagem de microsserviços possibilizar o uso de várias câmeras
simplesmente criando diversos serviços, a abordagem de multi feed viabiliza um processo
responsável dessas câmeras, o que pode ser interessante em cenários em que a
quantidade de câmeras são numerosas e a quantidade de recursos disponíveis limitada.
Dessa forma, há mais flexibilidade na hora de escalar a aplicação, sendo possível
a escalabilidade horizontal, ou seja, adicionando mais instâncias do serviço, ou, verticalmente,
aumentando a quantidade de recursos para cada instância.

Isso é possível por meio de técnicas de paralelismo, permitindo que o serviço
execute várias tarefas simultaneamente ao utilizar threads ou processos.
A escolha do tipo de paralelismo depende de diversos fatores,
como a quantidade de recursos disponíveis, a quantidade de câmeras
que o serviço vai ser responsável, a quantidade de imagens que serão capturadas
por segundo, entre outros.

Na linguagem de programação python, a biblioteca \textit{threading} possibilita
a criação de threads, que são responsáveis por executar tarefas em paralelo.
Porém, a execução de threads em python se apresenta limitada, dado que o interpretador
global do python é single-threaded como discutido no trabalho de \citeonline{python-thread}, ou seja, ele somente pode executar uma thread
por vez. Contudo, é possível utilizar processos, com isso, cabe ao sistema
operacional lidar com a optimização da execução.

Qualquer que seja a forma de paralelismo escolhida,
existe uma thread principal responsável por atividades comuns a
todas as threads, como a leitura do arquivo de configuração e o
servidor flask. Nessa thread principal, também é criada essas outras
threads, que por fim são responsáveis por executar as tarefas
de captura e publicação das imagens.

\begin{figure}[H]
    \caption{\label{camera-presence-thread-run} Criação de threads}
    \begin{minted}[linenos,xleftmargin=20pt]{python}
def run(self):
    self.client = mqtt.Client()
    self.client.connect(
        self.mqtt_server_address,
        self.mqtt_server_port, 60
    )

    t1 = Thread(target=self.worker, daemon=True)
    t1.start()
    \end{minted}
\end{figure}

Uma vez que temos uma thread principal e várias threads de captura, será preciso
uma forma de comunicação para que as threads de captura possam enviar
informações como o frame capturado. Para isso, o python disponibiliza uma
estrutura de dados thread-safe em forma de fila. Assim, ao iniciar a aplicação
para cada câmera, é criada uma fila e adicionado em uma lista de filas.
Acessando essa lista, pode-se obter a fila de uma câmera específica e, deste modo, os frames capturados por ela.

\subsection{Debug}
Para auxiliar no desenvolvimento e na depuração da aplicação foi criada uma
série de ferramentas para a visualização do fluxo de execução, do processo de
detecção de faces, do processo de tracking, além de disponibilizar métricas importantes
para saber se o código está otimizado e se a aplicação está funcionando corretamente.

\begin{figure}[h]
    \caption{\label{diagrama-debug}
        Diagrama de opções de debug
    }
    \begin{center}
        \includegraphics[scale=0.8]{./fig/diagrams/architecture_diagram-debug.png}
    \end{center}
    \legend{Fonte: Acervo pessoal}
\end{figure}

A primeira ferramenta concerne no monitoramento através do servidor flask
 (\autoref{flask-camera-presence}), dessa forma o usuário pode visualizar a
captura da imagem, os rostos detectados e o tracking em tempo real mediante
um navegador web. Essa função foi fundamental no desenvolvimento, pois
ela gera um feedback que possibilita verificar o correto funcionamento dos algoritmos, como
também realizar ajustes e experimentos que ajudam a compreender o funcionamento
do HAAR Cascade.

\begin{figure}[H]
    \caption{\label{flask-camera-presence}
        Flask debug através de um navegador web
    }
    \begin{center}
        \includegraphics[scale=0.35]{./fig/flask_camera_presence.png}
    \end{center}
    \legend{Fonte: Acervo pessoal}
\end{figure}

Uma outra ferramenta que auxiliou no desenvolvimento foi a possibilidade de
salvar os rostos identificados como imagens em disco \autoref{write_to_disk}, para que fosse analisado
a qualidade e frequência no qual os rostos seriam enviados para o
outro microsserviço, sem a necessidade de ter um broker MQTT funcionando.

\begin{figure}[H]
    \caption{\label{write_to_disk}
        Rostos identificados pelo camera presence
    }
    \begin{center}
        \includegraphics[scale=0.45]{./fig/resultados/output_images_camera_presence_sala_205.png}
    \end{center}
    \legend{Fonte: Acervo pessoal}
\end{figure}

Além disso, quando em modo de debug, a aplicação coleta uma série de métricas
e as imprime no terminal em um intervalo de tempo configurável de acordo
com os frames. Essas métricas foram utilizadas
principalmente para garantir que o código tenha desempenho suficiente para
processar os frames capturados. A \autoref{camera_presence_metrics} mostra
um exemplo de saída do terminal.

\begin{figure}[H]
    \caption{\label{camera_presence_metrics} Métricas \textit{camera presence}}
    \begin{minted}{text}
    expected_time = 4.0
    {
        'total_time': 2.8617095947265625,
        'time_to_get_frame': 1.8995428085327148,
        'time_to_resizing': 0.0024526119232177734,
        'time_to_gray': 0.034329891204833984,
        'time_to_losing_tracking': 0.5300185680389404,
        'time_to_detecting': 0.3070998191833496,
        'time_to_mqtt': 1.6689300537109375e-05,
        'time_to_flask': 0,
        'time_to_write': 0.06084132194519043
    }
    number_of_trackers = 4
    total_delay = -1.1382904052734375
    \end{minted}
\end{figure}

Para isso, a abordagem foi medir o tempo gasto em cada parte importante
do código. Assim, foi criado um dicionário que armazena o tempo gasto
acumulado. As formulas utilizadas para calcular o tempo gasto são:

\begin{equation}
\label{eqn:time_to_x}
time\_to\_x = end\_time - start\_time
\end{equation}

\begin{equation}
\label{eqn:total_time}
total\_time = \sum_{}^{} time\_to\_x
\end{equation}

Como dito, o intervalo no qual esse report é feito é configurado utilizando
a variável \textit{frames\_to\_report}, com isso, sabendo o \textit{framerate}
da câmera, é possível calcular o tempo esperado de processamento para cada
report e, então, calcular o total de delay acumulado.

\begin{equation}
\label{eqn:frames_to_report}
frames\_to\_report = 60
\end{equation}

\begin{equation}
\label{eqn:expected_time}
expected\_time = \frac{frames\_to\_report}{camera\_fps}
\end{equation}

\begin{equation}
\label{eqn:total_delay}
total\_delay = \sum_{}^{} (total\_time - expected\_time)
\end{equation}


Por fim, é obtido o \textit{total\_delay}, a soma de todos os delays.
Esse valor é importante para saber se o código está funcionando
corretamente, visto que se o valor for negativo, significa que o código
funcionará mais rápido do que o esperado, logo o processamento
está adequado, e se o valor for positivo o código funcionará mais lento do que o esperado.


\subsection{Detecção de rostos e Tracking}
Pode-se dizer que a detecção de faces e o tracking são as partes mais
importantes do \textit{camera presence}. É nesse processo em que os frames da
câmera são processados, e então identificada a presença de rostos.
Na figura \autoref{face_detection_code} é possível ver o trecho do código no
qual foi passado o frame (em preto e branco) e os argumentos para calibrar o
detector. O método \textit{detectMultiScale} retorna uma lista de tuplas,
em que cada tupla representa as coordenadas de um rosto encontrado.

\begin{figure}[H]
    \caption{\label{face_detection_code} Código de detecção de faces}
    \begin{minted}[linenos,xleftmargin=20pt]{python}
gray = cv2.cvtColor(baseImage, cv2.COLOR_BGR2GRAY)

faces = faceCascade.detectMultiScale(
    gray,
    scaleFactor=1.8,
    minNeighbors=5,
    minSize=(60, 60),
)
for (_x,_y,_w,_h) in faces:
    \end{minted}
\end{figure}

Como mostra a figura \autoref{camera-presence-flowchart}, o procedimento de
obter as faces presentes no frame através do HAAR Cascade é apenas um passo
do loop de processamento do frame. Isso ocorre pelo fato de que não é necessário
enviar para o outro microsserviço todos rostos encontrados em todos
frames, uma vez que praticamente todas imagens seriam repetidas e, além do mais,
a quantidade de dados seria muito grande para processar e transmitir.

\begin{figure}[H]
    \caption{\label{camera-presence-flowchart} Fluxo de execução do \textit{camera presence}}
    \begin{center}
        \includegraphics[scale=0.7]{./fig/camera_presence_flow.png}
    \end{center}
    \legend{Fonte: Acervo pessoal}
\end{figure}

Dessa forma, foi criado um sistema de \textit{tracking} para que apenas as
faces que são novas sejam enviadas para o outro microsserviço. O \textit{tracking}
é feito utilizando a biblioteca \textit{dlib} e funciona da seguinte forma:
a aplicação possui uma lista de \textit{trackers}, cada tracker consiste das coordenadas
de uma face obtida anteriormente junto da imagem dessa face. Antes de inciar o
procedimento de reconhecimento é realizada uma verificação de qualidade dos trackers.
Esse procedimento consiste em avaliar se aquela região monitorada no novo frame corresponde
a região salva de frames anteriores, caso a região seja suficientemente diferente
esse tracker será deletado (\autoref{tracker_deletion_code}).

\begin{figure}[H]
    \caption{\label{tracker_deletion_code} Código de remoção de trackers}
    \begin{minted}[linenos,xleftmargin=20pt]{python}
for fid in self.face_trackers.keys():
    trackingQuality = self.face_trackers[fid].update(baseImage)

    if trackingQuality < 7:
        # Delete tracker
    \end{minted}
\end{figure}

Somente após esse processo, realiza-se a detecção de faces no frame atual e, em seguida,
a verificação se a face é nova ou não. Para tal propósito, compara-se
a imagem da face detectada e as imagens salvas nos trackers,
verificando se o centro da região da face encontra-se dentro de algum
tracker.

\subsection{MQTT Publish}
Finalmente, após detectar e verificar que se trata de um novo rosto,
pode ser publicada a imagem em um tópico MQTT como mostra a \autoref{mqtt_publish_code}.

\begin{figure}[H]
    \caption{\label{mqtt_publish_code} Método de publicação no \textit{MQTT}}
    \begin{minted}[linenos,xleftmargin=20pt]{python}
def publish_to_mqtt(self, image):
    et, crop_img = cv2.imencode('.jpg', image)

    event['data']['image'] = base64.b64encode(crop_img).decode('utf-8')

    self.client.publish(
        'camera_presence/'+ self.name + '/feed',
        json.dumps(event),
        qos=0,
        retain=False
    )
    \end{minted}
\end{figure}

Para possibilitar a publicação de imagens foi necessário adequá-las
para o formato \textit{base64}, que é uma codificação
de dados que converte dados binários em uma sequência de caracteres ASCII.
Esse formato é utilizado para transmitir dados binários por meio de
protocolos de texto, como o \textit{MQTT}.


