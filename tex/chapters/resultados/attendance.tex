\section{Attendance}
O microsserviço \textit{attendance}, finalmente, encarrega-se
de realizar a frequência dos alunos a
partir das imagens disponibilizadas via protocolo MQTT.
A frequência calculada durante a aula gerará um relatório no seu término que
será disponibilizado ao professor. Neste capítulo será apresentado
detalhes sobre a implementação.

\begin{figure}[H]
    \caption{\label{attendance-diagram} Funcionamento básico do attendance}
    \begin{center}
        \includegraphics[scale=0.7]{./fig/diagrams/architecture_diagram-attendance_system.png}
    \end{center}
    \legend{Fonte: Acervo Pessoal}
\end{figure}

Como pode ser observado na \autoref{attendance-diagram}, o módulo attendance é composto
por três componentes principais: Web API, Comparação Rostos e Report. Em suma, o
Web API é a implementação de uma API REST que disponibiliza um endpoint para iniciar a verificação
de frequência. Já o Comparação Rostos é responsável por realizar o reconhecimento e
identificação das faces e, com isso, registrar a frequência dos alunos. E o Report é o local em que
produz-se o relatório.

\begin{figure}[H]
    \caption{\label{attendance-diagram} Diagrama de arquitetura do módulo attendance}
    \begin{center}
        \includegraphics[scale=0.7]{./fig/architecture_diagram-attendance.drawio.png}
    \end{center}
    \legend{Fonte: Acervo Pessoal}
\end{figure}

A API REST tem como parâmetros o \textit{idTurma}, \textit{idCamera} e o
\textit{tempoIntervalo}. Eles são necessários para iniciar
o processo de reconhecimento de faces. O \textit{idTurma} é usado para buscar as
informações da turma no banco de dados, como os alunos cadastrados e as imagens
deles. Ao mesmo tempo que o \textit{idCamera} busca as imagens no tópico referente
aquela câmera via protocolo MQTT. Também o \textit{tempoIntervalo} define
o intervalo de tempo entre as verificações de frequência.

\subsection{Recepção MQTT}
No início do processo de reconhecimento de faces via API, o sistema inicia uma
thread que fica aguardando as imagens serem recebidas via MQTT. Ao contrario do camera presence, na qual
cada \textit{feed} possui um cliente MQTT, nesse microsserviço existe apenas uma thread.
Quando um evento chega no tópico no qual o cliente está inscrito, esses dados são colocados em uma fila
que é consumida por outra thread que realiza o reconhecimento de faces (ver \autoref{attendance-work}).

\begin{figure}[H]
    \caption{\label{attendance-work} Thread de trabalho}
    \begin{minted}[linenos,xleftmargin=20pt]{python}
def work(self):
  data = self.mqtt_queue.get(block=True, timeout=30),
  face = self.decode_base64_image(data['data']['image'])

  self.check_attendance(face)
    \end{minted}
\end{figure}

É importante notar também que assim como no \textit{camera presence} a imagem foi codificada em \textit{base64} antes
de ser enviada. Quando recebido o evento precisamos transformar o \textit{payload} em uma imagem novamente,
fazendo o \textit{decode} dela. Com a imagem já em formato binário carregada podemos proceder
para a próxima etapa.

\subsection{Comparação de faces}
Na etapa de comparação de faces, foi utilizada a biblioteca \textit{DeepFace}. Trata-se de uma biblioteca
que funciona como \textit{front} para diversos algoritmos de detecção e reconhecimento facial. Ademais, todo o
processo de reconhecimento facial foi feito no microsserviço anterior, e usado somente a funcionalidade
de comparação de faces. Entre os diversos algoritmos disponíveis foi escolhido o \textit{Facenet512}.

\begin{figure}[H]
    \caption{\label{attendance_compare_images} Verificação de frequência}
    \begin{minted}[linenos,xleftmargin=20pt]{python}
def compare_images(face, reference):
  try:
    result = DeepFace.verify(
      face,
      reference,
      model_name='Facenet512',
      distance_metric='euclidean_l2'
    )
    return result

  except Exception as e:
    print('Error verifying face: ' + str(e))

  return False
    \end{minted}
\end{figure}

A \autoref{attendance_compare_images} mostra o \textit{wrapper} implementado em cima do \textit{DeepFace} para
realizar a comparação. A função \textit{compare\_images} recebe como argumento a imagem obtida do camera
presence e uma imagem de referência, que é a de um aluno cadastrado na turma.

\subsection{Registro de frequência}
A verificação de presença é efetuada a partir da comparação das imagens recebidas
com as imagens cadastradas. Para esse objetivo, é utilizado o algoritmo de comparação
de faces do DeepFace (ver \autoref{attendance_check_attendance}). Essa verificação é feita com todas as imagens
cadastradas do aluno e, caso seja positiva, a frequência é registrada. É assumido um intervalo de tempo
entre as verificações, caso o aluno já tenha sido marcado como presente dentro desse intervalo,
a frequência não é registrada novamente. Consequentemente, é contabilizado o número de vezes que o estudante
foi visto ao longo desses intervalos e, no final da aula, é multiplicado o número de vezes que
ele foi visto pelo tamanho do intervalo, totalizando o tempo presente dele naquela aula.

\begin{figure}[H]
    \caption{\label{attendance_check_attendance} Verificação de frequência}
    \begin{minted}[linenos,xleftmargin=20pt]{python}
def check_attendance(face):
  for student in classroom.students:
    for image in student.photos:
      image = '/workspace/storage/photos/' + image.hash_id + '.png'
      res = compare_images(face, image)

      if(res and res['verified'] == True):
        if (time.time() - student.last_seen) / 60 > attendance_time:
          student.last_seen = time.time()
          student.n_attendance += 1

          return student
        else:
          return False

  return False
    \end{minted}
\end{figure}

Por último, é fornecido um relatório com a frequência dos alunos. Esse relatório
é salvo no banco de dados e disponibilizado para download no formato CSV. No entanto,
o relatório é gerado apenas quando o processo de reconhecimento de faces é finalizado.
Para isso, é necessário que o módulo attendance não receba nenhuma imagem via MQTT por
um determinado tempo e que a duração da aula chegue ao fim.
