FROM ubuntu:22.04

RUN apt-get update && \
    apt-get -y install --no-install-recommends \
    locales \
    texlive \
    texlive-publishers \
    texlive-lang-portuguese \
    texlive-latex-extra \
    texlive-fonts-recommended \
    latexmk \
    lmodern \
    openssh-client \
    chktex \
    git \
    pip \
    python-is-python3

RUN pip install Pygments

# Set the locale
RUN sed -i '/en_US.UTF-8/s/^# //g' /etc/locale.gen && \
    locale-gen

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
